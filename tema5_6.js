var maxElem = 0;

(function () {
var table = document.createDocumentFragment();

var arr = [];
for (var i = 0; i < 3; i++) {
var tr = document.createElement('tr');
arr[i] = [];
for(var j = 0; j < 4; j++) {
var td = document.createElement('td');
td.innerHTML = arr[i][j] = getRandom();
tr.appendChild(td);
}
table.appendChild(tr);
}
document.getElementById('matrix').appendChild(table);

function getRandom(){
var min = 0;
var max = 9;
return Math.floor(Math.random()*(max-min))+min;
}
})();